package com.github.veromca.tcc.account.service.impl;

import org.dromara.hmily.annotation.Hmily;
import com.github.veromca.tcc.account.service.InLineService;
import org.springframework.stereotype.Component;

/**
 * The type In line service.
 *
 * @author veromca(Myth)
 */
@Component
public class InLineServiceImpl implements InLineService {

    @Override
    @Hmily(confirmMethod = "confirm", cancelMethod = "cancel")
    public void test() {
        System.out.println("执行inline try......");
    }

    /**
     * Confrim.
     */
    public void confirm() {
        System.out.println("执行inline confirm......");
    }

    /**
     * Cancel.
     */
    public void cancel() {
        System.out.println("执行inline cancel......");
    }
}
