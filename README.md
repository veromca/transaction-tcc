TCC概念

TCC是分布式事务中比较常用的一种方式，分别对应Try、Confirm、Cancel三种操作，这三种操作的业务含义如下：
- Try: 预留业务资源
- Confirm: 确认并执行业务操作
- Cancel: 取消执行业务操作
在一个跨服务的业务操作中，首先业务发起方通过Try锁住服务中的业务资源进行资源预留，只有资源预留成功了，后续操作才能正常进行。
Confirm操作是在Try之后进行，对Try阶段锁定的资源进行执行业务操作,类似于传统事务中的commit操作。
Cancel操作是在操作异常或者失败时进行回滚的操作，类似于传统事务的rollback。
在整个TCC方案中需要相关业务方分别提供TCC对应的功能，从而保证事务的强一致性，要么全部成功，要么全部回滚。

spring-cloud-transaction-tcc介绍

基于springcloud(Finchley.RELEASE)，hmily实现的TCC(Try-Confirm-Cancel)补偿型分布式事务解决方案。

#### 软件架构
软件架构说明
本项目模拟用户下单流程，用户通过订单服务（tcc-order）进行下单并支付;订单系统下单成功后，调用用户账户服务(tcc-account)和仓库服务(tcc-inventory)的Try功能，分别对用户资金和产品库存做锁定。
Try成功之后，分布式事务系统自动调用各种的Confirm接口进行执行操作；如果执行成功则流程正常结束，用户订单支付成功；
假如库存服务在Try阶段出现错误，分布式事务系统则调用各自的Cancel接口，将预留的资源进行回归；
类似的Confirm操作也是这个流程，一旦失败则走Cancel进行回滚，保证分布式系统中各个服务数据的一致性。

架构示意图

![输入图片说明](https://images.gitee.com/uploads/images/2019/1211/102108_ad173459_764599.png "tcc.png")
